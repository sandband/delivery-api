package com.sandun.delivery.order.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.sandun.delivery.order.model.core.DeliveryOrder;

@RepositoryRestResource(collectionResourceRel = "delivery-order-repository", path = "delivery-order-repository")
public interface DeliveryOrderRepository extends PagingAndSortingRepository<DeliveryOrder, Long> {

}
