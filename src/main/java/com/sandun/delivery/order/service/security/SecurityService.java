package com.sandun.delivery.order.service.security;

public interface SecurityService {

  public Boolean hasProtectedAccess();

}
