package com.sandun.delivery.order.service.authentication;

import com.sandun.delivery.order.exception.UnauthorisedAccessException;
import com.sandun.delivery.order.model.security.AuthencatedUser;

public interface AuthenticationService {

    public AuthencatedUser authenticate(String authToken) throws UnauthorisedAccessException;

}
