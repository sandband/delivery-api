package com.sandun.delivery.order;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Import;

import com.sandun.delivery.order.config.DeliveryOrderApplication;

/**
 * Run as a micro-service, registering with the Discovery Server (Eureka).
 * <p>
 * Note that the configuration for this application is imported from
 * {@link DeliveryOrderApplication}. This is a deliberate separation of
 * concerns and allows the application to run:
 * 
 * @author Sandun Lewke
 */
@EnableDiscoveryClient
@EnableAutoConfiguration
@Import(DeliveryOrderApplication.class)
public class ApplicationServer {

	protected Logger logger = Logger.getLogger(ApplicationServer.class.getName());

	/**
	 * Run the application using Spring Boot and an embedded servlet engine.
	 *
	 */
	public static void main(String[] args) {

	    	System.setProperty("spring.config.name", "delivery-api");
		SpringApplication.run(ApplicationServer.class, args);
	}
}
