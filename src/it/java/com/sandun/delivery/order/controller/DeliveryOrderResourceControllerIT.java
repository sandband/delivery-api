package com.sandun.delivery.order.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import com.sandun.delivery.order.ApplicationServer;
import com.sandun.delivery.order.config.DeliveryOrderApplication;
import com.sandun.delivery.order.model.core.DeliveryOrder;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DeliveryOrderApplication.class , ApplicationServer.class}, webEnvironment=WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application.properties")
public class DeliveryOrderResourceControllerIT {

	@Autowired
	private TestRestTemplate restTemplate  ;

	@Test
	public void testRetrieveDeliveryOrder() {

		String serviceUrl = "/delivery-api/delivery-order/5";
		ResponseEntity<DeliveryOrder> responseDeliveryOrder = restTemplate.getForEntity(serviceUrl, DeliveryOrder.class);
		DeliveryOrder deliveryOrder = responseDeliveryOrder.getBody();
		
		assertEquals(deliveryOrder.getProfileId(), "sandun22333");
		assertEquals(deliveryOrder.getFirstname(), "jorge22333");

	}


	@Test(expected = HttpClientErrorException.class)
	public void testNonExistingDeliveryOrder() {

		String serviceUrl = "/delivery-api/delivery-order/001";

		restTemplate.getForEntity(serviceUrl, DeliveryOrder.class);

	}

	@Test
	public void testCreateNewClientProfle() {

		String serviceUrl = "/delivery-api/delivery-order/";

		DeliveryOrder requestDeliveryOrder = new DeliveryOrder();
		requestDeliveryOrder.setProfileId(111L);
		requestDeliveryOrder.setFirstname("Client_test");
		requestDeliveryOrder.setSurname("Surname test");

		ResponseEntity<DeliveryOrder> responseDeliveryOrder = restTemplate.postForEntity(serviceUrl, new HttpEntity<DeliveryOrder>(requestDeliveryOrder), DeliveryOrder.class);

		assertThat(responseDeliveryOrder.getStatusCode(), is(HttpStatus.CREATED));
		
		DeliveryOrder deliveryOrder = responseDeliveryOrder.getBody();
		assertEquals(deliveryOrder.getFirstname(), "Client_test");
		assertEquals(deliveryOrder.getSurname(), "Surname test");
		

	}

}
